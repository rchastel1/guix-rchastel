(define-module (rchastel packages c++)
  #:use-module ((gnu packages base) #:select (glibc))
  #:use-module ((gnu packages build-tools) #:select (meson/newer))
  #:use-module ((gnu packages cmake) #:select (cmake))
  #:use-module ((gnu packages gcc) #:select (gcc))
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system meson)
  )

(define-public toml++
  (package
   (name "toml++")
   (version "3.4.0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/marzer/tomlplusplus.git")
           (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256 (base32 "1hvbifzcc97r9jwjzpnq31ynqnj5y93cjz4frmgddnkg8hxmp6w7"))))
   (build-system meson-build-system)
   (arguments
    (list #:meson meson/newer
          #:build-type "plain"
          #:configure-flags #~'("--auto-features" "enabled"
                                "--wrap-mode"     "nodownload"
                                "--python.bytecompile" "1"
                                "-D" "b_pie=true")))
   (native-inputs (list cmake meson/newer))
   (inputs `(,glibc (,gcc "lib")))
   (home-page "https://marzer.github.io/tomlplusplus/")
   (synopsis "TOML config parser and serializer for C++")
   (description "A C++17 TOML 1.0 parser and serializer")
   (license license:expat)))
