(define-module (rchastel packages python-web)
  #:use-module ((gnu packages check)
                #:select (python-pytest
                          python-pytest-timeout
                          python-pytest-asyncio
                          python-pytest-xprocess))
  #:use-module ((gnu packages compression)
                #:select (python-brotli))
  #:use-module ((gnu packages libffi) #:select (python-cffi))
  #:use-module ((gnu packages python-build)
                #:select (python-pluggy
                          python-poetry-core
                          python-typing-extensions))
  #:use-module ((gnu packages python-compression)
                #:select (python-brotlicffi))
  #:use-module ((gnu packages python-check) #:select (python-pytest-trio))
  #:use-module ((gnu packages python-crypto)
                #:select (python-cryptography
                          python-trustme))
  #:use-module ((gnu packages python-xyz)
                #:select (python-anyio
                          python-babel
                          python-click
                          python-flit
                          python-greenlet
                          python-iniconfig
                          python-jinja2
                          python-markupsafe
                          python-packaging
                          python-psutil
                          python-pycparser))
  #:use-module ((gnu packages python-web)
                #:select (hypercorn
                          python-flask
                          python-flask-babel
                          python-httpcore
                          python-httpx
                          python-socks python-socksio
                          python-starlette
                          python-uvicorn
                          python-werkzeug
                          python-yarl
                          ))
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module ((rchastel packages python-xyz)
                #:select (python-babel-2.15
                          python-blinker-1.6
                          python-itsdangerous-2.1
                          python-watchdog-4.0))
  )

(define-public python-ephemeral-port-reserve
  (package
   (name "python-ephemeral-port-reserve")
   (version "1.1.4")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "ephemeral_port_reserve" version))
     (sha256 (base32 "1chl9hil7ggz6l4sfhmp0l2j55qcskbc3pj9360b0309jwndmxxq"))))
   (build-system python-build-system)
   (native-inputs (list python-pytest))
   (home-page "https://github.com/Yelp/ephemeral-port-reserve")
   (synopsis "Bind to an ephemeral port, force it into the TIME_WAIT state, and unbind it.")
   (description "Sometimes you need a networked program to bind to a port that can't be hard-coded. Generally this is when you want to run several of them in parallel; if they all bind to port 8080, only one of them can succeed.

The usual solution is the 'port 0 trick'. If you bind to port 0, your kernel will find some arbitrary high-numbered port that's unused and bind to that. Afterward you can query the actual port that was bound to if you need to use the port number elsewhere. However, there are cases where the port 0 trick won't work. For example, mysqld takes port 0 to mean 'the port configured in my.cnf'. Docker can bind your containers to port 0, but uses its own implementation to find a free port which races and fails in the face of parallelism.

ephemeral-port-reserve provides an implementation of the port 0 trick which is reliable and race-free.")
   (license license:expat)))

(define-public python-werkzeug-3.0
  (package
   (inherit python-werkzeug)
   (version "3.0.3")
   (source
    (origin
     (inherit (package-source python-werkzeug))
     (uri (pypi-uri "werkzeug" version))
     (sha256 (base32 "065xv1yzdk1cir2ffrijgnm0c7a8xxni8ic5dgdaiazhm7ymnzh9"))))
   (build-system pyproject-build-system)
   (native-inputs
    (list python-cffi
          python-cryptography
          python-ephemeral-port-reserve
          python-flit
          python-greenlet
          python-iniconfig
          python-packaging
          python-pluggy
          python-psutil
          python-pycparser
          python-pytest
          python-pytest-timeout
          python-pytest-xprocess
          python-watchdog-4.0))
   (propagated-inputs (list python-markupsafe))))

(define-public python-flask-3.0
  (package
   (inherit python-flask)
   (version "3.0.3")
   (source
    (origin
     (inherit (package-source python-flask))
     (uri (pypi-uri "flask" version))
     (sha256 (base32 "0hk8yw721km9v75lbi0jhmdif1js2afxk918g5rs4gl2yc57pcnf"))))
   (build-system pyproject-build-system)
   (native-inputs (list python-flit python-pytest))
   (propagated-inputs
    (list python-werkzeug-3.0
          python-jinja2
          python-itsdangerous-2.1
          python-click
          python-blinker-1.6))))

(define-public python-flask-babel-4.0
  (package
   (inherit python-flask-babel)
   (version "4.0.0")
   (source
    (origin
     (inherit (package-source python-flask-babel))
     (uri (pypi-uri "flask_babel" version))
     (sha256 (base32 "14x508k9lkhgnmxdgg9wg7mr454fx6b68s0ii9kqfjizg81b9snv"))
     (modules '((guix build utils)))
     (snippet
      #~(substitute* "pyproject.toml" ;; Avoid to update pytz/pytzdata/tzdata
          (("pytz = \">=2022.7\"") "pytz = \">=2022.1\"")))))
   (build-system pyproject-build-system)
   (arguments (list #:tests? #f))
   (native-inputs (list python-poetry-core))
   (propagated-inputs
    (modify-inputs (package-propagated-inputs python-flask-babel)
      (replace "python-babel" python-babel-2.15)))))

(define-public python-tiny-proxy
  (package
   (name "python-tiny-proxy")
   (version "0.2.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/romis2012/tiny-proxy.git")
           (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256 (base32 "0lqlz4zw61rhq3nrk1dd0ynrw49pagcril7wr5zxkchflzvg9m77"))))
   (build-system python-build-system)
   (arguments
    (list #:configure-flags #~'("--optimize=1")
          #:tests? #f)) ; FIXME: No tests since it needs python-httpx-socks (cycles)
   (propagated-inputs (list python-anyio))
   (home-page "https://github.com/romis2012/tiny-proxy")
   (synopsis "Simple proxy (SOCKS4(a), SOCKS5(h), HTTP tunnel) server.")
   (description "Simple proxy (SOCKS4(a), SOCKS5(h), HTTP tunnel) server built with anyio. It is used for testing python-socks, aiohttp-socks and httpx-socks packages.")
   (license license:asl2.0)))

(define-public python-httpcore-0.17.3
  (package
   (inherit python-httpcore)
   (version "0.17.3")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/encode/httpcore")
           (commit version)))
     (file-name (git-file-name (package-name python-httpcore) version))
     (sha256
      (base32 "11pcijc04i2h3q935qfhxpih9h01f9p0bijf2afww16daag4knv4"))))))

(define-public python-httpx-socks
  (package
   (name "python-httpx-socks")
   (version "0.9.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/romis2012/httpx-socks.git")
           (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256 (base32 "0l3cn6alj6az1qd0x3a54yfx4m4j6m1lfc5zz7mbhcrd25zl7zpn"))))
   (build-system python-build-system)
   (arguments
    (list
     #:phases #~(modify-phases %standard-phases
                  (add-before 'install 'set-python-hash-seed
                    (lambda _ (setenv "PYTHONHASHSEED" "0"))))
     #:configure-flags #~'("--optimize=1" "--skip-build")))
   (native-inputs
    (list hypercorn
          python-pytest-asyncio python-pytest-trio
          python-typing-extensions python-brotli python-brotlicffi
          python-yarl
          python-trustme
          python-uvicorn python-socksio
          python-tiny-proxy
          python-starlette))
   (propagated-inputs
    (list python-socks python-httpcore-0.17.3 python-httpx))
   (home-page "https://github.com/romis2012/httpx-socks")
   (synopsis "Proxy (HTTP, SOCKS) transports for httpx")
   (description "The httpx-socks package provides proxy transports for httpx
client. SOCKS4(a), SOCKS5(h), HTTP (tunneling) proxy supported. It uses
python-socks for core proxy functionality.")
   (license license:asl2.0)))
