(define-module (rchastel packages python-build)
  #:use-module ((rchastel packages c++) #:select (toml++))
  #:use-module ((gnu packages python) #:select (python))
  #:use-module ((gnu packages python-build) #:select (python-setuptools))
  #:use-module ((gnu packages python-xyz) #:select (pybind11-2.10))
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system python)
  )

(define-public python-toml++
  (package
   (name "python-toml++")
   (version "1.0.14")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/bobfang1992/pytomlpp.git")
           (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256 (base32 "1i2lpfy2j024z4pr3jpnnv1pscaawvmvpwd15qzrkh2vl3czz8pg"))
     (modules '((guix build utils)))
     (snippet
      #~(substitute* "include/pytomlpp/pytomlpp.hpp"
         (("#include <tomlplusplus\\/include\\/toml\\+\\+\\/toml\\.h>")
          "#include <toml++/toml.h>")))))
   (build-system python-build-system)
   (arguments
    (list
     #:configure-flags #~'("--optimize=1" "--skip-build")))
   (native-inputs (list python-setuptools toml++))
   (propagated-inputs (list python pybind11-2.10))
   (home-page "https://github.com/bobfang1992/pytomlpp")
   (synopsis "A python wrapper for toml++.")
   (description "This is an python wrapper for toml++ (https://marzer.github.io/tomlplusplus/).!

Some points you may want to know before use:

   - Using toml++ means that this module is fully compatible with TOML v1.0.0.
   - We convert toml structure to native python data structures (dict/list etc.) when parsing, this is more inline with what json module does.
   - The binding is using pybind11.
   - The project is tested using toml-test and pytest.
   - We support all major platforms (Linux, Mac OSX and Windows), for both CPython and Pypy and all recent Python versions. You just need to pip install and we have a pre-compiled binaries ready. No need to play with clang, cmake or any C++ toolchains.
")
   (license license:expat)))
