(define-module (rchastel packages python-crypto)
  #:use-module ((gnu packages python-crypto) #:select (python-certifi))
  #:use-module (guix build-system python)
  #:use-module (guix packages)
  )

(define-public python-certifi-2024.6.2
  (package
   (inherit python-certifi)
   (version "2024.6.2")
   (source
    (origin
     (inherit (package-source python-certifi))
     (uri (pypi-uri "certifi" version))
     (sha256 (base32 "05kmfina92ppv8ysiyqp3aniil4g7783m7fnk5cdrpm7dwf3zm1w"))))))
