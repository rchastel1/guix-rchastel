(define-module (rchastel packages time)
  #:use-module ((gnu packages time) #:select (python-dateutil))
  #:use-module (guix build-system python)
  #:use-module (guix packages)
  )

(define-public python-dateutil-2.9
  (package
   (inherit python-dateutil)
   (version "2.9.0.post0")
   (source
    (origin
     (inherit (package-source python-dateutil))
     (uri (pypi-uri "python-dateutil" version))
     (sha256 (base32 "1lqak92ka6p96wjbf3zr9691gm7b01g7s8c8af3wvqd7ilh59p9p"))
     (patches '())))
   (arguments
    `(#:phases (modify-phases %standard-phases
                 (replace 'check
                   (lambda* (#:key tests? #:allow-other-keys)    
                     (when tests?
                       ;; Delete tests that depend on "freezegun" to avoid a
                       ;; circular dependency.
                       (delete-file "tests/test_utils.py")
                       (delete-file "tests/test_rrule.py")
                       
                       ;; XXX: Fails to get timezone from /etc/localtime.
                       (delete-file "tests/test_tz.py")
                     
                       (invoke "pytest" "-vv")))))))))
