(define-module (rchastel packages python-xyz)
  #:use-module ((gnu packages check)
                #:select (python-flaky
                          python-pytest
                          python-pytest-asyncio))
  #:use-module ((gnu packages python-build) #:select (python-setuptools))
  #:use-module ((gnu packages python-xyz)
                #:select (python-async-timeout
                          python-babel
                          python-blinker
                          python-itsdangerous
                          python-markdown-it-py
                          python-setproctitle
                          python-watchdog
                          ))
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  )

(define-public python-async-timeout-4.0.3
  (package
   (inherit python-async-timeout)
   (version "4.0.3")
   (source
    (origin
     (inherit (package-source python-async-timeout))
     (uri (pypi-uri "async-timeout" version))
     (sha256
      (base32
       "0bsj4z28ggxh1b6h6dvhx2mk6yqgb88bg8lyslpd10jdx1mxjh26"))))))

(define-public python-markdown-it-py-3.0
  (package
   (inherit python-markdown-it-py)
   (version "3.0.0")
   (source
    (origin
     (inherit (package-source python-markdown-it-py))
     (uri (pypi-uri "markdown-it-py" version))
     (sha256 (base32 "1swgvyiavak0nmfb31lq5zck5chwhmyf6qb6qwpcav86zaa0mxp3"))))))

(define-public python-setproctitle-1.3.3
  (package
   (inherit python-setproctitle)
   (version "1.3.3")
   (source
    (origin
     (inherit (package-source python-setproctitle))
     (uri (pypi-uri "setproctitle" version))
     (sha256 (base32 "1bna0wb1hvdij2xpz2qrh0l1jh47r8ipl0zz6xw5c0gawx8y24y9"))))))

(define-public python-blinker-1.6
  (package
   (inherit python-blinker)
   (version "1.6.2")
   (source
    (origin
     (inherit (package-source python-blinker))
     (uri (pypi-uri "blinker" version))
     (sha256 (base32 "04z2mgkw69dns42ip5qmpvf1fp2mpqf7mysrfl3giagkdvk3vzaa"))))
   (build-system pyproject-build-system)
   (native-inputs (list python-pytest python-pytest-asyncio))
   (propagated-inputs (list python-setuptools))))

(define-public python-itsdangerous-2.1
  (package
   (inherit python-itsdangerous)
   (version "2.1.2")
   (source
    (origin
     (inherit (package-source python-itsdangerous))
     (uri (pypi-uri "itsdangerous" version))
     (sha256 (base32 "0shmfj4c67xgdawxxri2zqxzrhsxaiiif0pr4zrl4pky665wdfsx"))))))

(define-public python-watchdog-4.0
  (package
   (inherit python-watchdog)
   (version "4.0.1")
   (source
    (origin
     (inherit (package-source python-watchdog))
     (uri (pypi-uri "watchdog" version))
     (sha256 (base32 "0i4bgwphhy47n11pzda5018fcbk03ql8s0k7i0g529gsfkvarfpf"))))
   (arguments
    (list
     #:test-flags #~(list "-k" "not test_kill_auto_restart and not test_auto_restart_on_file_change_debounce")))
   (native-inputs
    (modify-inputs (package-native-inputs python-watchdog)
      (append python-flaky)))))

(define-public python-babel-2.15
  (package
   (inherit python-babel)
   (version "2.15.0")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "babel" version))
     (sha256 (base32 "04s4l0q2yak231f67k18qfpj78g9449wy553qz38nxh5blk0xbwd"))))))
