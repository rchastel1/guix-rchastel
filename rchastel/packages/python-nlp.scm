(define-module (rchastel packages python-nlp)
  #:use-module ((gnu packages python-xyz) #:select (pybind11))
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix build-system python))

(define-public python-fasttext-predict
  (package
   (name "python-fasttext-predict")
   (version "0.9.2.2")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/searxng/fasttext-predict.git")
           (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256 (base32 "015c8ak7hxyaz3rsjjzz6zdhjiz3c4im00j21s18n9gafyp0jv06"))))
   (build-system python-build-system)
   (arguments
    (list #:configure-flags #~'("--optimize=1")))
   (propagated-inputs
    (list pybind11))
   (home-page "https://github.com/searxng/fasttext-predict")
   (synopsis " fasttext with wheels and no external dependency, but only the predict method (<1MB)")
   (description "Python package for fasttext:

    - keep only the predict method, all other features are removed

    - standalone package without external dependency (numpy is not a dependency)

    - wheels for various architectures using GitHub workflows. The script is inspired by lxml build scripts.
")
   (license license:expat)))
