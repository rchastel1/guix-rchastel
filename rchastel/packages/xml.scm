(define-module (rchastel packages xml)
  #:use-module ((gnu packages xml) #:select (python-lxml))
  #:use-module (guix build-system python)
  #:use-module (guix packages))

(define-public python-lxml-5.2
  (package
   (inherit python-lxml)
   (version "5.2.2")
   (source
    (origin
     (inherit (package-source python-lxml))
     (uri (pypi-uri "lxml" version))
     (sha256 (base32 "11yvrzlswlh81z6lpmds2is2jd3wkigpwj6mcfcaggl0h64w8bdv"))))))
