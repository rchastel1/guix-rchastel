(define-module (rchastel packages compression)
  #:use-module ((gnu packages compression) #:select (brotli))
  #:use-module (guix build-system python)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  )

(define-public brotli-1.1
  (package
   (inherit brotli)
   (version "1.1.0")
   (source
    (origin
     (inherit (package-source brotli))
     (uri (git-reference
           (url "https://github.com/google/brotli")
           (commit (string-append "v" version))))
     (file-name (git-file-name (package-name brotli) version))
     (sha256
      (base32 "0cvcq302wpjpd1a2cmxcp9a01lwvc2kkir8vsdb3x11djnxc0nsk"))))))

(define-public python-brotli-1.1
  (package
   (inherit brotli-1.1)
   (name "python-brotli")
   (build-system python-build-system)
   (arguments '())
   (synopsis "Python interface to Brotli")
   (description "This package provides a Python interface to the @code{brotli}
package, an implementation of the Brotli lossless compression algorithm.")))
